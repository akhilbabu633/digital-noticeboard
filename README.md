# Digital Noticeboard for College - Django Project.

[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Requirements](#requirements)
- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Configuration](#configuration)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Introduction

This is a digital noticeboard project built using Django. It serves as a platform for colleges to share announcements, news, and important information with their students and staff. This README provides a guide on setting up and using the system.

## Features

- User authentication (Login and Registration).
- Role-based access control (Admin, Faculty, Student).
- Create, edit, and delete notices.
- Categorized notices (e.g., Academic, Events, General).
- Search and filter notices.
- Notification system for new notices.
- Responsive design for both mobile and desktop.

## Requirements

- Python 3.6+
- Django 3.0+
- Git
- PostgreSQL or SQLite (for the database, you can choose either)

## Getting Started

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/akhilbabu633/digital-noticeboard.git

2. Change directory to the project folder:

    ```bash
    cd Digital_noticeboard/

3. Create a virtual environment and activate it:

    ```bash
    python3 -m venv venv
    source venv/bin/activate
4. Install project dependencies:

    ```bash
    pip install -r requirements.txt

5. Migrate the database and create a superuser:

    ```bash
    python manage.py migrate
    python manage.py createsuperuser

6. Start the development server:
    
    ```bash
    python manage.py runserver
Your digital noticeboard should now be accessible at http://localhost:8000.

## Usage

- Access the admin interface at http://localhost:8000/admin/ to manage users, categories, and notices.

- Users can log in and view or create notices based on their roles.



